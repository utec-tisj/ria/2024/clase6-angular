import { Component, OnInit } from '@angular/core';
import { Hospital } from 'src/models/hospital';
import { HospitalesService } from 'src/services/hospitales.service';

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styleUrls: ['./hospitales.component.scss']
})
export class HospitalesComponent implements OnInit{

  public hospitals: Hospital[] = [
    {id : 1, nombre : 'Hospital A'}, 
    {id : 2, nombre : 'Hospital B'}, 
    {id : 3, nombre : 'Hospital C'}
  ];

  public hospitals2: Hospital[] = [];

  constructor(private hospitalesService: HospitalesService) {     
  }

  ngOnInit(): void {
    this.hospitalesService.getHospitals().subscribe((hospitals) => {
      this.hospitals2 = hospitals;
    });
  }
}
